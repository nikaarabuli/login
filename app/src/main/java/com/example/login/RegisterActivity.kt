package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {
    private lateinit var RegisterPassword: EditText
    private lateinit var editTextEmail: EditText
    private lateinit var buttonRegister1: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListeners()
    }
    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        RegisterPassword = findViewById(R.id.RegisterPassword)
        buttonRegister1 = findViewById(R.id.buttonRegister1)

    }
    private fun registerListeners(){
        buttonRegister1.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = RegisterPassword.toString()

            if(email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this,RegisterActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                    }

                }

                }
    }

}