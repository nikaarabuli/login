package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var ButtonRegister: Button
    private lateinit var buttonLogin: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registerListeners()
    }
    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        ButtonRegister = findViewById(R.id.ButtonRegister)
        buttonLogin = findViewById(R.id.buttonLogin)

    }
    private fun registerListeners(){
        ButtonRegister.setOnClickListener{
        val intent = Intent(this,RegisterActivity::class.java)

        }
        buttonLogin.setOnClickListener {

        val email = editTextEmail.text.toString()
        val password = editTextPassword.toString()
            startActivity(Intent(this,RegisterActivity::class.java))
            finish()

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
            return@setOnClickListener
        }
        }
    }
}